const {Client, Server} = require('nextcloud-node-client');

const server = new Server(
    {
        basicAuth:
            {
                password: "",
                username: "",
            },
        url: "",
    });

const client = new Client(server);

// let si = client.getSystemInfo();
// si.then(result => {
//     console.log(result);
// }).catch(error => {
//     console.error('Got error', error);
// });

// Get files
// const rootFolder = client.getRootFolder();
const folderName = '/DCIM/Camera';
client.getFolder(folderName).then(folder => {
    folder.getFiles().then(files => {
        console.log('Total files :', files.length);

        let img = 0, vid = 0, other = 0;
        let years = new Map();
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            // console.log(i, file.name, file.baseName, file.id, file.mime, file.size);

            let name = file.baseName;
            if (name.startsWith("IMG_")) {
                img += 1;
                // console.log(name);
                // if (name.match(/IMG_\d+_\d+[_ \w()]*\.jpg/g))
                //     img1 += 1;
                let year = name.substr(4, 4);
                let month = name.substr(8, 2);
                // let count = 0;
                let months;
                if (years.has(year)) {
                    // count = years.get(year);
                    months = years.get(year);
                } else {
                    months = new Map();
                }
                let fileList;
                if (months.has(month)) {
                    fileList = months.get(month);
                } else {
                    fileList = [];
                }
                fileList.push(file);
                months.set(month, fileList);
                years.set(year, months);
                // console.log(name, month, year)
            } else if (name.startsWith("VID")) {
                vid += 1;
                let year = name.substr(4, 4);
                let month = name.substr(8, 2);
                let months;
                if (years.has(year)) {
                    months = years.get(year);
                } else {
                    months = new Map();
                }
                let fileList;
                if (months.has(month)) {
                    fileList = months.get(month);
                } else {
                    fileList = [];
                }
                fileList.push(file);
                months.set(month, fileList);
                years.set(year, months);
            } else {
                other += 1;
            }
        }
        console.log('IMG', img);
        console.log('VID', vid);
        console.log('Other', other);

        // console.log
        for (let [year, months] of years.entries()) {
            console.log(year, months.size);
            folder.hasSubFolder(year).then(async result => {
                console.log('Folder', year, 'availability', result);
                if (!result) {
                    await folder.createSubFolder(year);
                }
                let yearFolderName = folderName + '/' + year;
                let yearFolder = await client.getFolder(yearFolderName);
                for (let [month, files] of months.entries()) {
                    console.log('Month', month, files.length);
                    if (!await yearFolder.hasSubFolder(month)) {
                        console.log('Month folder is not available', month);
                        await yearFolder.createSubFolder(month);
                        console.log('Created month folder', month);
                    }
                    let monthFolderName = yearFolderName + '/' + month;
                    //moving to folder
                    // let movingQueue = [];
                    for (let fileCounter in files) {
                        let file = files[fileCounter];
                        console.log('Moving file', file.baseName, file.size);
                        let resultFile = await file.move(monthFolderName + '/' + file.baseName);
                        console.log('File moved to', resultFile.name);
                        // movingQueue.push(file.move(monthFolderName + '/' + file.baseName));
                        // if (movingQueue.length >= 10) {
                        //     console.log('Waiting for 10 in month', month, year);
                        //     await Promise.all(movingQueue);
                        //     console.log('Moving of 10 in month', month, 'year', year, 'completed');
                        //     movingQueue = [];
                        // }
                    }
                    // console.log('Waiting for month', month, year, ' remaining');
                    // await Promise.all(movingQueue);
                    // console.log('Moving of month', month, 'year', year, 'completed');
                }

            }).catch(error => {
                console.error(error);
            });
        }


    }).catch(reason => {
        console.error(reason);
    });
}).catch(reason => {
    console.error(reason);
});