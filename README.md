Nodejs script to organize photo/videos to the proper directory based on year and month if filename follows pattern
 - Moves photos and videos to proper directory like android Nextcloud client
#### Filename pattern
Image - IMG_yyyymmdd_<alphanumeric>.jpg
Video - VID_yyyymmdd_<alphanumeric>.mp4
#### Usage
Install Dependencies
 - Install nodejs if not installed on your system
 - Goto the project directory and run `npm install`
Open index.js
 - Enter url, username, password
 - Enter source path (Default: /DCIM/Camera)
 